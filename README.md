# README #

## DATABÁZE ##
### Výchozí konfigurace ###
**název databáze: ** userapp

**uživatel: ** postgres

**heslo: ** 1234

**konfigurační soubor na serveru: ** user-table-server/src/main/resources/application.properties

příklad zprovoznění na ubuntu

```
sudo apt-get install postgresql-client
sudo -u postgres psql postgres
\password postgres
sudo -u postgres createdb userapp
sudo -u postgres psql < ./create_table_users.sql
```
viz. [PostgreSQL - Community Help Wiki](https://help.ubuntu.com/community/PostgreSQL)
## Klient ##
**Port: ** 3000
```
#!bash
cd user-table-client
npm install && npm start
```
## Server ##
**Port: ** 8080
```
#!bash
cd user-table-server
mvn install && java -jar target/user-table-server-1.0.jar
```