package cz.morosystems.usertable.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(name = "name")
    @NotNull
    @Size(min=3, max=40)
    private String name;
    
    @Column(name = "username")
    @NotNull
    @Size(min=3, max=25)
    private String username;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @Column(name = "password", updatable=false)
    @Size(min=3, max=60)
    private String password;
    
    protected User() {}

    public User(String name) {
        this.name = name;
    }

    public User(User user) {
		this.id = user.id;
		this.name = user.name;
		this.username = user.username;
		this.password = user.password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@JsonIgnore
	public String getRole() {
		return "USER";
	}


	@Override
    public String toString() {
        return String.format(
                "User[id=%d, name='%s', username='%s', password='%s']",
                id, name, username, password);
    }
}
