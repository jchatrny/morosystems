package cz.morosystems.usertable.error;

public class UserNotFoundException extends UserException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4177801196721360922L;

	public UserNotFoundException(String message){
		super(message);
	}
}
