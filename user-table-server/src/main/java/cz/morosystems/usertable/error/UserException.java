package cz.morosystems.usertable.error;

public class UserException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UserException(String message) {
        super(message);
    }
	
	public String toJSON(){
		return "{\"message\":  \""+this.getMessage()+"\" }}";
	}
}
