package cz.morosystems.usertable.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cz.morosystems.usertable.error.UserException;
import cz.morosystems.usertable.model.User;
import cz.morosystems.usertable.service.UserService;

@RestController
public class UserController {
	
	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	
	/**
	 * Get all users
	 * @return
	 */
    @RequestMapping("/")
    public List<User> index() {
        return userService.getAllUsers();
    }
    
    /**
     * Get single user by id.
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public ResponseEntity<Object> getUser(@PathVariable Long id) {
    	User user;
    	try{
    		user = userService.getUserById(id);
    	}
    	catch(UserException e){
    		return ResponseEntity.status(HttpStatus.CONFLICT).body(e.toJSON());
    	}
    	
        return ResponseEntity.ok(user);
    }
    
    /**
     * Insert user
     * @param user
     * @return
     */
    @RequestMapping(method=RequestMethod.POST, value="/")
    public ResponseEntity<Object> addUser(
    		@RequestBody @Valid User user, 
    		BindingResult bindingResult) {
    	//validation
        if (bindingResult.hasErrors()) {
        	log.info("Validation of "+ user +": "+ bindingResult.getAllErrors());
        	return ResponseEntity.ok(bindingResult.getAllErrors());
        }

    	try{
    		return ResponseEntity.ok(userService.addUser(user));
    	}
    	catch(Exception e){
	        log.info("Exception in addUser: "+e.getCause());
	        log.info("Exception in addUser: "+e.getMessage());

	        return  new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	    }
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="/{id}")
    public ResponseEntity<Object> updateUser(
    		@PathVariable Long id,
    		@RequestBody @Valid User user,	
    		BindingResult bindingResult) {
    	//validation
        if (bindingResult.hasErrors()) {
        	log.info("Validation of "+ user +": "+ bindingResult.getAllErrors());
        	return ResponseEntity.ok(bindingResult.getAllErrors());
        }
        log.info("Updating "+ user);
    	try{
    		return ResponseEntity.ok(userService.updateUser(id, user));
    	}
    	catch(UserException e){
    		return new ResponseEntity<Object>(e.getMessage(),HttpStatus.CONFLICT); //.body(e.toJSON());
    	}
    	catch(Exception e){
	        log.info("Exception in addUser: "+e.getCause());
	        log.info("Exception in addUser: "+e.getMessage());

	        return  new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	    }
    }
   

}