package cz.morosystems.usertable.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;

import cz.morosystems.usertable.model.User;
import cz.morosystems.usertable.service.UserService;

@RestController
public class UserSecuredController {
	
	final static String NOT_AUTH_MESSAGE = "Pro danou operaci je vyžadováno přihlášení.";
	
	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	
    @RequestMapping(method=RequestMethod.DELETE, value="/secured/deleteUser/{id}")
    public ResponseEntity<Object> deleteUser(
    		@PathVariable Long id) {
    	List<User> users;
    	
    	try{
    		 users = userService.deleteUser(id);
    	}
    	catch(Exception e){
    		return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    	}
    	log.info("Deleted user with id:" + id);
		return ResponseEntity.ok().body(users);
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/secured/")
    public ResponseEntity<Object> test(){
    	return ResponseEntity.ok().body(RequestContextHolder.currentRequestAttributes().getSessionId());
	}
}
