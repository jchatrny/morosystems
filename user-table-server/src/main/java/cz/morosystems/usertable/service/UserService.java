package cz.morosystems.usertable.service;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cz.morosystems.usertable.controller.UserController;
import cz.morosystems.usertable.error.UserNotFoundException;
import cz.morosystems.usertable.model.User;
import cz.morosystems.usertable.repo.UserRepository;


@Service
public class UserService implements UserDetailsService{
	
	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserRepository userRepository;
	
	

	public List<User> getAllUsers() {
		//List<User> users = new ArrayList<>();
		//userRepository.findAll().forEach(users::add);
		return userRepository.findAllByOrderByIdAsc();
	}


	public User getUserById(Long id) throws UserNotFoundException {
		User user = userRepository.findOne(id);
		if(user == null){
			throw new UserNotFoundException("Uživatel s id=\""+id+"\" nenalezen.");
		}
		return user;

	}


	public User addUser(User user) {
		return userRepository.save(user);
	}


	public User updateUser(Long id, User user) throws UserNotFoundException {
		this.getUserById(id);
		return userRepository.save(user);
		
	}


	public List<User> deleteUser(Long id) {
		userRepository.delete(id);
		return this.getAllUsers();	
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if(user.equals(null)){
			throw new UsernameNotFoundException("Uživatel s username=\""+username+"\" nenalezen.");
		}else{
			CustomUserDetails ud = new CustomUserDetails(user,Arrays.asList("ROLE_USER"));
			log.info("user logged: "+ ud);
			return ud;
		}
	}
}
