package cz.morosystems.usertable.repo;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cz.morosystems.usertable.model.User;

@Repository
@Qualifier(value = "userRepository")
public interface UserRepository extends CrudRepository<User, Long> {
	public List<User> findAllByOrderByIdAsc();
	public User findByUsername(String username);
}
