\c userapp
DROP TABLE users;
CREATE TABLE users (
id SERIAL PRIMARY KEY NOT NULL,
name varchar(40),
username varchar(25),
password varchar(60)
);

INSERT INTO users (id,name,username,password) values (1, 'Raz', 'Raz', 'Raz');
INSERT INTO users (id,name,username,password) values (2, 'Dva', 'Dva', 'Dva');

SELECT * FROM users;
