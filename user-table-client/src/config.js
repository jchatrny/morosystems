const config = {
	server: {
		url: 'http://localhost:8080/',
		port: 8080,
		headers: { 	
			Accept: 'application/json',
			'Content-Type': 'application/json',
		}
	},
	titles: {
		cz: {
			id: "id",
			name: "Jméno",
			username: "Uživatelské jméno",
			password: "Heslo",
		}
	},
	error:{
		cz: {
			connectionError: "Připojení k serveru se nezdařilo. ",
			unknownReponse: "Neznámá odpověď od serveru.",
			"403": "Pro danou operaci je vyžadováno přihlášení.",
			"401": "Pro danou operaci je vyžadováno přihlášení."
		}
	}

}

export default config;