import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route,  browserHistory} from "react-router";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from "redux";
import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux'

import thunk from 'redux-thunk';

import { reducer } from './action/actions';
import LoginContainer from './container/LoginContainer';
import MainAppContainer from './container/MainAppContainer';
import './css/material.min.css';
import './css/index.css';



const middleware = [thunk, routerMiddleware(browserHistory)];
const store = createStore(
  combineReducers({
    system: reducer,
    routing: routerReducer
  }), applyMiddleware(...middleware)
)
//const store = createStore(reducer, applyMiddleware(thunk));

//store.dispatch({ type: "ADD_USER", payload: "Bob" });

const history = syncHistoryWithStore(browserHistory, store);

const Routing = () => {
	return(
		<Provider store={store}>
			<Router history={history} >			
				<main>
					<Route exact path="/" component={MainAppContainer} />
					<Route exact path="/login" component={LoginContainer} />
				</main>
			</Router>
		</Provider>
		);

};

ReactDOM.render(
  <Routing />,
  document.getElementById('root')
);
