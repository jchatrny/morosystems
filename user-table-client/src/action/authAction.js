import axios from 'axios';
import config from '../config';
import {push} from 'react-router-redux';
import md5 from "react-native-md5";

import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from "./actions"

export const login = (username, password) => {
  //console.log(bcrypt.hashSync(password, bcrypt.genSaltSync(10)));
  return (dispatch, getState) => {
    const passHash = md5.hex_md5(password);
    axios.get(config.server.url+"secured/", 
      {credentials: 'cross-origin',
      auth: {
        username, 
        password: passHash
        }
      }
    )
    .then((response) => {
        
        //console.log(response);
        if (response.status === 200 && Boolean(response.data) && response.data.length > 0) {
            //we got session 
            /*
            console.log("we got session: "+response.data);
            localStorage.setItem("token", response.data);
            cookie.save('JSESSIONID', response.data, { path: '/' });
            */
            localStorage.setItem("username", username);
            localStorage.setItem("password", passHash);
            dispatch({type: LOGIN_SUCCESS});
            dispatch(push("/"));
        }

    }).catch((error) => {
        dispatch({type: LOGIN_FAIL});
      });
  }
}

export const logout = () => {
  return (dispatch, getState) => {
      dispatch({type: LOGOUT});
  }
}
