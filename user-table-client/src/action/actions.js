import axios from 'axios';
import config from '../config';
import {push} from 'react-router-redux';
import md5 from "react-native-md5";


export const NEW_USER_SUCCESS = 'NEW_USER_SUCCESS';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const NEW_USER_FAIL = 'NEW_USER_FAIL';
export const EDIT_USER_FAIL = 'EDIT_USER_FAIL';
export const DELETE_USER_FAIL = 'DELETE_USER_FAIL';
export const GET_USERS_FAIL = 'GET_USERS_FAIL';
export const VALIDATION_FAIL = 'VALIDATION_FAIL';
export const LOGIN_FAIL = "LOGIN_FAIL";
export const SET_SELECTED_ITEM_ID = "SET_SELECTED_ITEM_ID";
export const LOGOUT = "LOGOUT";

const initState = {
	isLogged: false,
	loginFail: false,
	validationFail: false,
	responseFail: false,
	errorMessages: [],
	validationMessages: [],

	selectedItemId: null,
	data: [],
}
export const reducer = function(state = initState, action){
	switch(action.type){
		case LOGIN_SUCCESS:
			state = {
				...state, 
				isLogged: true, 
				loginFail: false, 
			};
			break;
		case LOGIN_FAIL:
			state = {...state, isLogged: false, loginFail: true};
			break;
		case LOGOUT:
		    localStorage.removeItem("username");
      		localStorage.removeItem("password");
			state = {...state, isLogged: false};
			break;
		case DELETE_USER_FAIL:
		case NEW_USER_FAIL:
		case EDIT_USER_FAIL:
		case GET_USERS_FAIL:
			console.log(action.payload);
			if(action.payload !== undefined)
			state = {
				...state,
				responseFail: true,
				errorMessages: action.payload instanceof Array ? action.payload : [action.payload],
			}
			break;
		case VALIDATION_FAIL:
			state = {
				...state,
				validationFail: true,
				validationMessages: action.payload,
			}
			break;
		case SET_SELECTED_ITEM_ID:
			state = {
				...state,
				selectedItemId: action.payload,
			}
			break;
		case NEW_USER_SUCCESS:
			state = {
				...state,
				validationFail: false,
				data: [...state.data, action.payload],
			}
			break;
		case EDIT_USER_SUCCESS:
			state = {
				...state,
				validationFail: false,
				data: state.data.map((item) => item.id === state.selectedItemId ? action.payload : item),
			}
			break;
		case GET_USERS_SUCCESS:
		case DELETE_USER_SUCCESS:
			//console.log(action.payload);
			state = {
				...state,
				data: action.payload,
			}
			break;
		default:
			break;
	}
	return state;
}


const getValidationMessage = (col, message) => {
	return ' Pole "' + config.titles.cz[col] + '" - ' + message;
}

export const getUsers = () => {
	return (dispatch, getState) => {
		axios.get(config.server.url, config.headers)
		.then((response) => { 

			const { data } = response; 
			if(Boolean(data) && data.length > 0){
				dispatch({type: GET_USERS_SUCCESS, payload: data});				
			}
			else{	
				dispatch({type: GET_USERS_FAIL, payload: "Prázdná odpověď."});	
			};
		})
		.catch((error) => {
			dispatch({type: GET_USERS_FAIL, payload: error});
		});
	}
}

export const newUser = (user) => {
	return (dispatch, getState) => {
		//validate passsword
		if(user.password.length < 3 ||  user.password.length > 25){
			//this.handleValidationError("password", );
			dispatch({type: VALIDATION_FAIL, payload: ["velikost musí být mezi 5 a 25"]});
			return false;
		}
		//hash password
		user.password = md5.hex_md5(user.password);

		//send request
		axios.post(config.server.url, user, config.server.headers)
		.then((response) => {
			
			//empty reposponse
			if(!Boolean(response.data) || response.data <= 0 ){		
				dispatch({type: NEW_USER_FAIL, payload: config.error.cz.unknownReponse});	
			}
			//validation violation
			else if(response.data[0] !== undefined && response.data[0].bindingFailure !== undefined){
				//newUser.password = unhashedPassword;
				//console.log(response.data);
				const messages = [];
				response.data.map( (data, index)  => 
					messages.push(getValidationMessage(data.field, data.defaultMessage))
				);
				dispatch({type: VALIDATION_FAIL, payload: messages});
			}
			//ok
			else{
				dispatch({type: NEW_USER_SUCCESS, payload: response.data});
			}
		    

	  	}).catch((error) => {
	  		dispatch({type: NEW_USER_FAIL, payload: error.response});	
	  	});
	}  

}

export const editUser = (user) => {
	return (dispatch, getState) => {
		const url = config.server.url+user.id;

		axios.put(url, user, config.server.headers).then((response) => {
			//empty reposponse
			if(!Boolean(response.data) || response.getData <= 0 ){
				dispatch({type: EDIT_USER_FAIL, payload: "Prázdná odpověď."});		
			}
			//validation violation
			else if(response.data[0] !== undefined && response.data[0].bindingFailure !== undefined){
				let messages = [];
				response.data.map( (data, index)  => 
					messages.push(this.getValidationMessage(data.field, data.defaultMessage))
				);
				dispatch({type: VALIDATION_FAIL, payload: messages});
			}
			//ok
			else{
				//console.log(response.data);
	    		dispatch({type: 'SET_SELECTED_ITEM_ID', payload: response.data.id});
	    		dispatch({type: EDIT_USER_SUCCESS, payload: response.data});
			}
		}).catch((error) => {
	  		dispatch({type: EDIT_USER_FAIL, payload: error});
	  	});
	}

}
export const deleteUser = (user) => {
	return (dispatch, getState) => {
		//delete form server
		const url = config.server.url+'secured/deleteUser/'+user.id;

		const headers = config.server.headers;
		//headers['Authorization'] = 'Bearer '+localStorage.getItem("token");
		//console.log(headers);
		//document.cookie = 'JSESSIONID='+localStorage.getItem("token");
		axios.delete(url,{
		      	auth: {
			        username: localStorage.getItem('username'), 
			        password: localStorage.getItem('password')//: bcrypt.hashSync(password, bcrypt.genSaltSync(10))
		        },
				headers
				}
			)
		.then((response) => {
			//console.log(response);
			const { data } = response; 
			if(Boolean(data) && data.length > 0 && response.status === 200){
				dispatch({type: DELETE_USER_SUCCESS, payload: data});

				//if is deleted row in edit mode, close it
				if(user.id === getState().system.selectedItemId) {
					dispatch({type: 'SET_SELECTED_ITEM_ID', payload: null});
				}
			}
			else{
				dispatch({type: DELETE_USER_FAIL, payload: response});
			}

		}).catch((error) => {
			//console.log(error);
			if(error.statusCode === undefined || error.statusCode === "401"){
			    localStorage.removeItem("username");
  				localStorage.removeItem("password");
				dispatch(push("/login"));
			}
			else{
	  			dispatch({type: DELETE_USER_FAIL, payload: error});
	  		}
	  		//redirect
	  		
	  	});
	}
}

export const setSelectedItemId = (id) => {
	return (dispatch, getState) => {
		dispatch({type: 'SET_SELECTED_ITEM_ID', payload: id});
	}
}



