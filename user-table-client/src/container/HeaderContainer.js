import React, { Component } from 'react';
import { Link } from 'react-router';

export default class HeaderContainer extends Component{
	render() {
		return(
		<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
		<header className="mdl-layout__header">
			<div className="mdl-layout__header-row">
				<span className="mdl-layout-title">{this.props.title}</span>
				<div className="login-block right">
					{
						(this.props.isLogged === true)
					 	? <span className="">{this.props.user} <a onClick={() => this.props.onLogout()}>Odhlásit</a></span>
					 	: <Link to="/login">Přihlásit</Link>
					}
				</div>
			</div>

		</header>

		</div>
		);
	}
}