import React, {Component} from "react";
import { connect } from "react-redux";

import { login } from '../action/authAction';
import LoginForm from '../component/form/LoginForm';
import StatusBar from '../component/common/StatusBar';

export class LoginContainer extends Component {
	constructor(props){
		super(props);
		this.state = {
			loginFormData: {
				username: "",
				password: "",
			}
		}
		
	}

	onChangeLogin(e){
		const key = e.target.name;
		const value = e.target.value;
		this.setState({
			loginFormData: {...this.state.loginFormData, [key]: value },
		});
	}


	onSubmitLogin(e){
		const username = this.state.loginFormData.username;
		const password= this.state.loginFormData.password; 	

		this.props.login(username, password);
		
	}

	render() {
		return (
			<div>
			<LoginForm
				username={this.state.loginFormData.username}
				password={this.state.loginFormData.password}
				title="Přihlášení"
				onSubmit={(e) => {this.onSubmitLogin(e)}}
				onChange={(e) => {this.onChangeLogin(e)}}
			/>
			{this.props.loginFail && <StatusBar type="warning" items={['Zadané uživatelské jméno a heslo nesouhlasí.	']} />}
			{this.props.responseFail && <StatusBar type="error" items={this.props.errorMessages}  />}
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
  isLogged: state.system.isLogged,
  loginFail: state.system.loginFail,
  responseFail: state.system.responseFail,

  errorMessages: state.system.errorMessages,
})

export default connect(mapStateToProps, {login})(LoginContainer)