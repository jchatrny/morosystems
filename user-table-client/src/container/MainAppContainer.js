import React, { Component } from 'react';
import { connect } from "react-redux";
import { getUsers, newUser, editUser, deleteUser, setSelectedItemId } from '../action/actions';
import { logout } from '../action/authAction';

import HeaderContainer from './HeaderContainer';
import UserTable from '../component/table/UserTable';
import AddRowForm from '../component/form/AddRowForm';
import EditRowForm from '../component/form/EditRowForm';
import StatusBar from '../component/common/StatusBar';

import config from '../config';

export class MainAppContainer extends Component {
	constructor(props){
		super(props);
		this.state = {
			onLoadError: false,
			//selectedItemId: null,
			editedItemData: {
				id: "",
				name: "", 
				username: "",
			},
			newItemData: {
				name: "", 
				username: "",
				password: "",
			},
		}

		
	}

	componentWillMount(){
		//Download data from server
		this.props.getUsers();
	}
	componentDidMount(){
		this.setState({
			...this.state,
			onLoadError: false,
		})
	}


	/**
	 * Handle add row event and comunication with server
	 */
	onSubmitAddRow(e){	
		const newUser = this.state.newItemData;
		this.props.newUser(newUser);
		if(!this.validationFail && !this.responseFail){
			this.setState({
				...this.state,
				newItemData: {
					name:  "",
					username:  "",
					password:  "",
				}
			});
			return true
		}
	  	return false;
	}

	/**
	 * Handle delete row event and comunication with server
	 */
	onDeleteRow(item){
		if (this.props.data.indexOf(item) > -1) {
 			this.props.deleteUser(item);
		}
	}

	/**
	 * Handle dit row event and comunication with server
	 */
	onSubmitEditRow(e){	
		this.props.editUser(this.state.editedItemData);

	}

	/**
	 * Handle when row of table is being selected
	 */
	onSelectRow(item){
		//console.log("handleSelectRowEdit"+item);
		this.setState({
			editedItemData: {
				id: item.id,
				name: item.name,
				username: item.username,
			},
		});
		this.props.setSelectedItemId(item.id);
		//console.log(item);
	}

	onChangeEditRow(e){
		const key = e.target.name;
		const value = e.target.value;
		this.setState({
			editedItemData: {...this.state.editedItemData, [key]: value},
		});
	}
	onChangeAddRow(e){
		const key = e.target.name;
		const value = e.target.value;
		this.setState({
			newItemData: {...this.state.newItemData, [key]: value},
		});
	}


	render(){
		//check error
		if(this.state.onLoadError === true){
			return(
				<div className="error">
					{config.error.cz.connectionError} 
				</div>
			);
		}
		var editedItemData = this.state.editedItemData;
		var newItemData = this.state.newItemData;
		//TODO: mMainAppContainering na keys
		const titles = Object.values(config.titles.cz);
		titles.splice(titles.indexOf("Heslo"),1);
		return (
				<div className="table-wrapperer wrapperer">
					<HeaderContainer 
						title="Tabulka uživatelů" 
						user={this.props.username}
						isLogged={this.props.isLogged}
						onLogout={() => this.props.logout()}/>
					<UserTable 
						titles={titles} 
						rows={this.props.data}
						selectedItemId={this.props.selectedItemId} 
						onDeleteRow={(item) => this.onDeleteRow(item)} 
						onSelectRow={(item) => this.onSelectRow(item)}/>
					<AddRowForm
						name={newItemData.name}
						username={newItemData.username}
						password={newItemData.password}
						onSubmit={(e) => {this.onSubmitAddRow(e);}}
						onChange={(e) => {this.onChangeAddRow(e)}}/>
					{this.props.selectedItemId &&
					<EditRowForm 
						id={editedItemData.id}
						name={editedItemData.name}
						username={editedItemData.username}
						onSubmit={(e) => {this.onSubmitEditRow(e);}}
						onChange={(e) => {this.onChangeEditRow(e)}}/>
					}
					{(this.props.validationFail) &&
						<StatusBar items={this.props.validationMessages} type="warning" />}		
					{(this.props.responseFail) &&
						<StatusBar items={this.props.errorMessages} type="error" />}			
				</div>
			);	
	}				
}

const mapStateToProps = (state) => ({
	username: localStorage.getItem("username"),
	password: localStorage.getItem("password"),
	isLogged: localStorage.getItem("username") !== null,
	validationFail: state.system.validationFail,
	responseFail: state.system.responseFail,
	validationMessages: state.system.validationMessages,
	errorMessages: state.system.errorMessages,

	selectedItemId: state.system.selectedItemId,

  	data: state.system.data,
})

MainAppContainer.propTypes = {
	username: React.PropTypes.string,
	password: React.PropTypes.string,
	isLogged: React.PropTypes.bool,
	validationFail: React.PropTypes.bool,
	responseFail: React.PropTypes.bool,
	validationMessages: React.PropTypes.array,
	errorMessages: React.PropTypes.array,

	selectedItemId: React.PropTypes.number,

  	data: React.PropTypes.array,
};

export default connect(mapStateToProps, {logout, getUsers, newUser, editUser, deleteUser, setSelectedItemId})(MainAppContainer)