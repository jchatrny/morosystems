import React, { Component } from 'react';

export default class StatusBar extends Component {

    render() {
		return (
      		<div className={"status mdl-shadow--2dp mdl-components__"+this.props.type}>

              {this.props.items.map(function(line,index){
                return (<p key={index}>{line}</p>);
              })}
      		</div>
  		); 
    }
}

StatusBar.propTypes = {
    items: React.PropTypes.array.isRequired,
    type: React.PropTypes.string.isRequired,
};

