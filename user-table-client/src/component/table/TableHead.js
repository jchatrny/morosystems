import React, { Component } from 'react';

export default class TableHead extends Component {
  render() {
    return (
        <thead>
            {this.props.children}
        </thead>
    );
  }
}