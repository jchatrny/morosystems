import React, { Component } from 'react';

export default class TableRow extends Component {
    render() {
        return (
          <tr className={this.props.isSelected ? "row-selected" : ""}>
            {this.props.children}
          </tr>
        );
    }

}