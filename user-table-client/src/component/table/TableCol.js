import React, { Component } from 'react';

export default  class TableCol extends Component {
    render() {
      //console.log(this.props.index);
      return this.props.tag === "th" ? (<th>{this.props.data}</th>) : (<td>{this.props.data}</td>);
    }
}
