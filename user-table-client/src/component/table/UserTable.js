import React, { Component } from 'react';
import TableHead from './TableHead';
import TableBody from './TableBody';
import TableRow from './TableRow';
import TableCol from './TableCol';


/**
 * Table
 */
class UserTable extends Component {
	render() {
		//add empty collumns to align count with tbody
		const titles = Object.values(this.props.titles);
		titles.push("","");
		return (
			<table id="user-table" className="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
				<TableHead>
					<TableRow>
						{Object.values(titles).map(function(col,index){	
							return <TableCol data={col} key={"c"+index} tag="th" />;
						})}
					</TableRow>
				</TableHead>
				<TableBody>
					{this.props.rows.map((row, index) => this.renderBodyRow(row, index))} 
				</TableBody>           
			</table>
		);
	}
	renderBodyRow(row, i){
		//add collumns with edit and delete
		const cols = Object.values(row);
		cols.push(this.addDeleteButton(row), this.addEditButton(row));
		return (
			<TableRow key={"r"+i} isSelected={this.props.selectedItemId  === row.id}>
				{cols.map(function(col,index){
					//console.log(data);
					return <TableCol data={col} key={"c"+index} tag="td" />;
				})}
			</TableRow>
		);
	}
	addDeleteButton(row){
      return (<button className="mdl-button delete-button"
                      key={"d"} 
                      onClick={() => {this.props.onDeleteRow(row)}}></button>)
    }
    addEditButton(row){
      return (<button className="mdl-button edit-button"
                      key={"e"} 
                      onClick={() => {this.props.onSelectRow(row)}}></button>)
    }
}

UserTable.propTypes = {
	rows: React.PropTypes.arrayOf(React.PropTypes.shape({
		id: React.PropTypes.number,
		name: React.PropTypes.string, 
		username: React.PropTypes.string, 
		password: React.PropTypes.string,  
	})).isRequired,
	selectedItemId: React.PropTypes.number,
	onDeleteRow: React.PropTypes.func,
	onSelectRow: React.PropTypes.func,
};


export default UserTable;
