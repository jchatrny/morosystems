import React from 'react';
import TextField from './TextField';

const AddRowForm = ({name, username, password, onSubmit, onChange}) => {
        return (
            <form onSubmit={(e) => {e.preventDefault();onSubmit(e);}}>
                <div className="add-row mdl-shadow--2dp">
                    {/*name*/}
                    <TextField
                        name="name"
                        value={name}
                        placeholder="Jméno"
                        onChange={(e) => onChange(e)}
                        />
                    {/*username*/}
                    <TextField
                        name="username"
                        value={username}
                        placeholder="Uživateské jméno"
                        onChange={(e) => onChange(e)}
                        />
                    {/*password*/}
                    <TextField
                        name="password"
                        type="password"
                        placeholder="Heslo"
                        value={password}
                        onChange={(e) => onChange(e)}
                        />
                    <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" 
                        type="submit" >
                        Přidat
                    </button>
                </div>
            </form>
        ); 
}

AddRowForm.propTypes = {
    name: React.PropTypes.string.isRequired, 
    username: React.PropTypes.string.isRequired, 
    password: React.PropTypes.string.isRequired,  
    onSubmit: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func.isRequired,
};

export default AddRowForm;
