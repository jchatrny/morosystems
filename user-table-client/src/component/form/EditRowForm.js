import React from 'react';
import TextField from './TextField';

const EditRowForm = ({ id, name, username, onSubmit, onChange}) => {
		return (
            <form onSubmit={(e) => {e.preventDefault();onSubmit(e);}}>
          		<div className="edit-row mdl-shadow--2dp">

                    {/*id*/}
                    <TextField
                        name="id"
                        value={id}
                        wrapperClass="small"
                        readOnly
                        onChange={(e) => onChange(e)}
                        />

                    {/*name*/}
                    <TextField
                        name="name"
                        value={name}
                        onChange={(e) => onChange(e)}
                        />
                    {/*username*/}
                    <TextField
                        name="username"
                        value={username}
                        onChange={(e) => onChange(e)}
                        />
                    <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" 
                        type="submit">
                        Upravit
                    </button>
          		</div>
            </form>
  		); 
}

EditRowForm.propTypes = {
    id: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]).isRequired,
    name:  React.PropTypes.string.isRequired,
    username: React.PropTypes.string.isRequired,
    onSubmit: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func.isRequired,
};

export default EditRowForm;