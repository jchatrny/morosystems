import React from 'react';

const TextField = ({ name, value, label, error, type, placeholder, readOnly, wrapperClass, onChange, checkUserExists }) => {
  return (
    <div className={'mdl-textfield mdl-js-textfield '+wrapperClass}>
      <input
        onChange={onChange}
        onBlur={checkUserExists}
        value={value}
        type={type}
        name={name}
        placeholder={placeholder}
        className="mdl-textfield__input"
        readOnly={readOnly}
      />
    {error && <span className="help-block">{error}</span>}
    </div>  );
}

TextField.propTypes = {
    name: React.PropTypes.string.isRequired,
    value: React.PropTypes.oneOfType([
            React.PropTypes.string,
            React.PropTypes.number
        ]).isRequired,
    label: React.PropTypes.string,
    error: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    readOnly: React.PropTypes.bool,
    wrapperClass: React.PropTypes.string,
    type: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func.isRequired,
    checkUserExists: React.PropTypes.func
}

TextField.defaultProps = {
    type: 'text',
    wrapperClass: '',
    placeholder: '',
    readOnly: false,
}

export default TextField;