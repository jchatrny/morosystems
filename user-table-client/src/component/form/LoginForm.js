import React from 'react';
import TextField from './TextField';

const LoginForm = ({title, username, password, onSubmit, onChange}) => {
		return (
				<div className="mdl-card mdl-shadow--6dp login-wrapper">
					<div className="mdl-card__title mdl-color--primary mdl-color-text--white">
						<h2 className="mdl-card__title-text">{title}</h2>
					</div>
					<form onSubmit={(e) => {e.preventDefault();onSubmit(e);}}>
				  		<div className="mdl-card__supporting-text">
		                    {/*username*/}
		                    <TextField
		                        name="username"
		                        value={username}
		                        placeholder="Uživatelské jméno"
		                        onChange={(e) => onChange(e)}
		                        />
		                   {/*password*/}
		                    <TextField
		                        name="password"
		                        type="password"
		                        value={password}
		                        placeholder="Heslo"
		                        onChange={(e) => onChange(e)}
		                        />
						</div>
						<div className="mdl-card__actions mdl-card--border">
							<button className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" 
							type="submit">Log in</button>
						</div>
					</form>
				</div>
  		); 
}

LoginForm.propTypes = {
	title:  React.PropTypes.string.isRequired,
    password:  React.PropTypes.string.isRequired,
    username: React.PropTypes.string.isRequired,
    onSubmit: React.PropTypes.func.isRequired,
};


export default LoginForm;